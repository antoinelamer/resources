# Record linkage

This repository provides some resources for the project [Fichiers décés INSEE](https://gitlab.com/antoinelamer/fichiers_deces_insee).
These files include data from raw INSEE files 2010-2020 available [here](https://www.data.gouv.fr/fr/datasets/fichier-des-personnes-decedees/).

**_light** files, provide 

| Field | Description | Available in _light file |
| -------- | -------- | -------- | 
| nom_src   |    | X |
| prenoms   |    | X |
| date_naissance   |    | X |
| lieu_naissance   |    |  |
| commune_naissance   |    | X |
| pays_naissance   |    |    |
| date_deces   |    | X |
| lieu_deces   |    |  |
| nom   |    | X |
| prenom_1   |    | X |
| prenom_1_2   |    | X |


| Files | Description |
| -------- | -------- | 
| deces.Rdata   | RData format    |
| deces.Rds  | Rds format    |
| deces.csv   |     |
| deces.pkl   | Pickle format for python program    |
| deces_light.Rdata   |     |
| deces_light.csv   |     |

"btn btn-sm btn-primary"